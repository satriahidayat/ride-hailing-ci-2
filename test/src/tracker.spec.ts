import { dbQuery, syncDB } from "./utils/db"
import { random } from "faker"
import { get as httpGet } from "request-promise-native"
import { expect } from "chai"

const TRACKER_HOST = process.env['TRACKER_HOST'] || 'localhost';
const TRACKER_PORT = process.env['TRACKER_PORT'] || 3000;


describe('Tracker Server', function() {
    this.timeout(30000);

    before(function(done) {
        setTimeout(done, 15000);
    })
   

    describe('Movement', function() {
        beforeEach(async function() {
            // await syncDB();

            //reset database
            await dbQuery({type: "remove", table: "track_events"})

            //feed data
            this.tracks = (await dbQuery({
                type: "insert",
                table: "track_events",
                values: {
                    "rider_id": random.number({min: 1, max:10}),
                    "north": random.number({min: 1, max: 30}),
                    "south": random.number({min: 1, max: 30}),
                    "west": random.number({min: 1, max: 30}),
                    "east": random.number({min: 1, max: 30}),
                    "createdAt": new Date(),
                    "updatedAt": new Date()
                },
                returning: ["rider_id", "north", "south", "east", "west", "createdAt"]
            }))[0][0];
        });

        it('Harusnya memberikan data suatu driver', async function() {
            const response = await httpGet(
                `http://${TRACKER_HOST}:${TRACKER_PORT}/movement/${this.tracks["rider_id"]}`,
                {json: true}
            );
            expect(response.ok).to.be.true;
            expect(response.logs).to.be.deep.equal(
                [{
                    time: this.tracks["createdAt"].toISOString(),
                    east: this.tracks["east"],
                    west: this.tracks["west"],
                    north: this.tracks["north"],
                    south: this.tracks["south"]
                }]
            );
        });
    });
    
    // describe('Listen', function() {
    //     it('Harusnya menambahkan suatu driver'), async function() {
    //         const listen = await post(

    //         )
    //     }
    // });
});